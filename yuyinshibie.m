function varargout = yuyinshibie(varargin)
% YUYINSHIBIE MATLAB code for yuyinshibie.fig
%      YUYINSHIBIE, by itself, creates a new YUYINSHIBIE or raises the existing
%      singleton*.
%
%      H = YUYINSHIBIE returns the handle to a new YUYINSHIBIE or the handle to
%      the existing singleton*.
%
%      YUYINSHIBIE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in YUYINSHIBIE.M with the given input arguments.
%
%      YUYINSHIBIE('Property','Value',...) creates a new YUYINSHIBIE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before yuyinshibie_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to yuyinshibie_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help yuyinshibie

% Last Modified by GUIDE v2.5 04-Jun-2019 12:49:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @yuyinshibie_OpeningFcn, ...
                   'gui_OutputFcn',  @yuyinshibie_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before yuyinshibie is made visible.
function yuyinshibie_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to yuyinshibie (see VARARGIN)

% Choose default command line output for yuyinshibie
handles.output = hObject;
setappdata(gcf,'isrecording',0)
global J;
J=0;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes yuyinshibie wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = yuyinshibie_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in record_start.
function record_start_Callback(hObject, eventdata, handles)
% hObject    handle to record_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% isrecording=getappdata(gcf,'isrecording');
% if ~isrecording
% setappdata(gcf,'isrecording',1);
% recorder(gcf,handles);
% else
% setappdata(gcf,'isrecording',0);
% end
fs=48000;
handles.recObj=audiorecorder(fs,16,1); %采样率，比特率，通道数
% set(handles.recObj,'StartFcn',{@recordstart_Callback,handles}, ...
%     'StopFcn',{@recordstop_Callback,handles});
record(handles.recObj); % 开始录音
guidata(hObject,handles);
global msg;
msg= msgbox('正在录音','提示信息');
set(msg,'NumberTitle', 'off', 'Name', '正在录音，请稍等……');

% --- Executes on button press in record_end.
function record_end_Callback(hObject, eventdata, handles)
% hObject    handle to record_end (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
J=J+1;
global msg;
global X0;
close(msg);
stop(handles.recObj); % 停止录音
% thehandles=handles;
handles.Sample=getaudiodata(handles.recObj);% 获取录音
guidata(hObject,handles);
X0{J,1}=handles.Sample;
plot(handles.axes1,cell2mat(X0(J,1)));
x=handles.Sample;
 %% ******************端点检测******************
x = double(x);
x = x / max(abs(x));
%常数设置
FrameLen = 256;%帧长为256点
FrameInc = 80;%帧移为80点
amp1 = 20;%初始短时能量高门限
amp2 = 2;%初始短时能量低门限
zcr1 = 10;%初始短时过零率高门限
zcr2 = 5;%初始短时过零率低门限
maxsilence = 8;  % 8*10ms  = 80ms
%语音段中允许的最大静音长度，如果语音段中的静音帧数未超过此值，则认为语音还没结束；如果超过了
%该值，则对语音段长度count进行判断，若count<minlen，则认为前面的语音段为噪音，舍弃，跳到静音
%状态0；若count>minlen，则认为语音段结束；
minlen  = 15;    % 15*10ms = 150ms
%语音段的最短长度，若语音段长度小于此值，则认为其为一段噪音
status  = 0;     %初始状态为静音状态
count   = 0;     %初始语音段长度为0
silence = 0;     %初始静音段长度为0
%计算过零率
x1=x(1:end-1);
x2=x(2:end);
%分帧
tmp1=enframe(x1,FrameLen,FrameInc);
tmp2=enframe(x2,FrameLen,FrameInc);
signs = (tmp1.*tmp2)<0;
diffs = (tmp1 -tmp2)>0.02;
zcr   = sum(signs.*diffs, 2);%一帧一个值
%计算短时能量
%一帧一个值
%amp = sum(abs(enframe(filter([1 -0.9375], 1, x), FrameLen, FrameInc)), 2);
amp = sum(abs(enframe(x, FrameLen, FrameInc)), 2);
%调整能量门限
amp1 = min(amp1, max(amp)/4);
amp2 = min(amp2, max(amp)/8);
%开始端点检测
%For循环，整个信号各帧比较
%根据各帧能量判断帧所处的阶段
x1 = 0;
x2 = 0;
v_num=0;%记录语音段数
v_Begin=[];%记录所有语音段的起点
v_End=[];%记录所有语音段的终点
%length(zcr)即为帧数
for n=1:length(zcr)
   goto = 0;
   switch status
   case {0,1}                   % 0 = 静音, 1 = 可能开始
      if amp(n) > amp1          % 确信进入语音段
         x1 = max(n-count-1,1);
%          '打印每个x1*FrameInc'
%          x1*FrameInc
         status  = 2;
         silence = 0;
         count   = count + 1;
      elseif amp(n) > amp2 | ... % 可能处于语音段
             zcr(n) > zcr2
         status = 1;
         count  = count + 1;
      else                       % 静音状态
         status  = 0;
         count   = 0;
      end
   case 2,                       % 2 = 语音段
      if amp(n) > amp2 | ...     % 保持在语音段
         zcr(n) > zcr2
         count = count + 1;
      else                       % 语音将结束
         silence = silence+1;
         if silence < maxsilence % 静音还不够长，尚未结束
            count  = count + 1;
         elseif count < minlen   % 语音长度太短，认为是噪声
            status  = 0;
            silence = 0;
            count   = 0;
         else                    % 语音结束
            status  = 3;
         end
      end
   case 3,
      %break;
      %记录当前语音段数据
      v_num=v_num+1;   %语音段个数加一
      count = count-silence/2;
      x2 = x1 + count -1;
      v_Begin(1,v_num)=x1*FrameInc; 
      v_End(1,v_num)=x2*FrameInc;
      %不跳出 数据归零继续往下查找下一段语音
      status  = 0;     %初始状态为静音状态
      count   = 0;     %初始语音段长度为0
      silence = 0;     %初始静音段长度为0
   end
end  

if length(v_End)==0
    x2 = x1 + count -1;
    v_Begin(1,1)=x1*FrameInc; 
    v_End(1,1)=x2*FrameInc;
end
lenafter=0;
for len=1:length(v_End)
    tmp=v_End(1,len)-v_Begin(1,len);
    lenafter=lenafter+tmp;
end
lenafter;
afterEndDet=zeros(lenafter,1);%返回去除静音段的语音信号
beginnum=0;
endnum=0; 
    for k=1:length(v_End)
        tmp=x(v_Begin(1,k):v_End(1,k));
        beginnum=endnum+1;
        endnum=beginnum+v_End(1,k)-v_Begin(1,k);
        afterEndDet(beginnum:endnum)=tmp; 
    end
  plot(handles.axes2,tmp);
   global X;
   X{J,1}=tmp;




function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in shibie.
function shibie_Callback(hObject, eventdata, handles)
% hObject    handle to shibie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global X;
global J;
x=cell2mat(X(J,1));
fs=48000;
fh=fs/2; % fs=8000Hz,fh=4000Hz    语音信号的频率一般在300-3400Hz，所以一般情况下采样频率设为8000Hz即可。
max_melf=2595*log10(1+fh/700);%耳朵响应频率
M=24;%三角滤波器的个数
N=floor(0.03*fs);%设置帧长
i=0:25;
f=700*(10.^(max_melf/2595*i/(M+1))-1);%将mei频域中的 各滤波器的中心频率 转到实际频率
F=zeros(24,N);
 for m=1:24
     for k=1:N
         i=fh*k/N;
         if (f(m)<=i)&&(i<=f(m+1))
             F(m,k)=(i-f(m))/(f(m+1)-f(m));
         else if (f(m+1)<=i)&&(i<=f(m+2))
                 F(m,k)=(f(m+2)-i)/(f(m+2)-f(m+1));
             else
                 F(m,k)=0;
             end
         end
     end
 end
 axes(handles.axes3); 
 plot((1:N)*fh/N,F);
 %%%%%%%%%%%%%%%DCT系数%%%%%%%%%%%
dctcoef=zeros(12,24);
 for k=1:12
   n=1:24;
   dctcoef(k,:)=cos((2*n-1)*k*pi/(2*24));
 end

 %%%%%%%%%%%对语音信号进行预加重处理%%%%%%%%%%
len=length(x);
 alpha=0.98;
 y=zeros(len,1);
 for i=2:len
     y(i)=x(i)-alpha*x(i-1);
 end
 %%%%%%%%%%%%%%%MFCC特征参数的求取%%%%%%%%%%%%
h=hamming(N);%256*1
 num=floor(0.25*N); %帧移
 count=floor((len-N)/num+1);%帧数
 c1=zeros(count,12);
 for i=1:count
     x_frame=y(num*(i-1)+1:num*(i-1)+N);%256*1
     w = x_frame.* h;%
     Fx=abs(fft(w));%     Fx=abs(fft(x_frame));
     s=log(F*Fx.^2);%取对数
     c1(i,:)=(dctcoef*s)'; %离散余弦变换 
 end
 %%%%%%%%%%%%差分系数%%%%%%%%%%%
dtm = zeros(size(c1));
 for i=3:size(c1,1)-2
   dtm(i,:) = -2*c1(i-2,:) - c1(i-1,:) + c1(i+1,:) + 2*c1(i+2,:);
 end
 dtm = dtm / 3;
 %%%%合并mfcc参数和一阶差分mfcc参数%%%%%
ccc = [c1 dtm];
 %去除首尾两帧，因为这两帧的一阶差分参数为0
ccc = ccc(3:size(c1,1)-2,:);
%  ccc=ccc(:,2:20);
%  save('moban8','ccc');
% fileFolder='E:\模式识别\语音识别\模板\';
fileFolder='模板\';
dirOutput=dir(strcat(fileFolder,'*'));
fileNames={dirOutput.name};
len = length(fileNames);
for I=3:len
    K_Trace = strcat(fileFolder, fileNames(I));
    eval(['y','=','load(K_Trace{1,1})',';']);
    Y{I-2,1}=cat(1,y.ccc);%按行读取
    D(I-2,1)=dtw(cell2mat(Y(I-2,1)),ccc);
end
min1=10^310;
k=-1;
for i=1:100
    if (D(i,1)<=min1)
        min1=D(i,1);
        k=i;
    end
end

global shu;
if min1>3*10^4
    msgbox('无法识别');
    J=J-1;
else
if k>0&&k<=10
    shu(J)=0;
elseif k>10&&k<=20
        shu(J)=1;
elseif k>20&&k<=30
        shu(J)=2;
elseif k>30&&k<=40
        shu(J)=3;
elseif k>40&&k<=50
        shu(J)=4;
elseif k>50&&k<=60
        shu(J)=5;
elseif k>60&&k<=70
        shu(J)=6;
elseif k>70&&k<=80
        shu(J)=7; 
elseif k>80&&k<=90
        shu(J)=8;        
elseif k>90&&k<=100
        shu(J)=9;         
end
c=num2str(shu(J));
n1=strcat(get(handles.edit1,'string')); % 获取数字号码
A=strcat(n1,c);  %连接每次识别出的号码
set(handles.edit1,'string',A); % 显示号码
end
% --- Executes on button press in send.
function send_Callback(hObject, eventdata, handles)
% hObject    handle to send (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global shu;
%% 客户机
echotcpip('off')
echotcpip('on',80)
t_client=tcpip('192.168.43.146',80,'NetworkRole','client');%与本地主机建立连接，端口号为80，作为客户机连接。
% set(t_client,'InputBufferSize',4500000);
% set(t_client,'OutputBufferSize',300);
set(t_client,'Timeout',300);
fopen(t_client);%与一个服务器建立连接，直到建立完成返回，否则报错。
data_send=[1 2 3];%发送的数字数据。
%%%%%%%% txt_send='HELLO'; %发送的文本数据
pause(1);%等待连接稳定，随意设置。
fwrite(t_client,data_send);%写入数字数据
%%%%%%%% fprintf(t_client,txt_send);%发送文本数据
fclose(t_client);
echotcpip('off')


% --- Executes on button press in repeal.
function repeal_Callback(hObject, eventdata, handles)
% hObject    handle to repeal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
global X;
global shu;
global X0;
X(J)=[];
shu(J)=[];
J=J-1;
A='';
if J>0
for i=1:J
    c=num2str(shu(i));
    A=strcat(A,c);
end
set(handles.edit1,'string',A); 
 plot(handles.axes1,cell2mat(X0(J)));
 plot(handles.axes2,cell2mat(X(J)));
else
    cla(handles.axes1);
cla(handles.axes2);
cla(handles.axes3);
set(handles.edit1,'string',''); 
end


% --- Executes on button press in clear.
function clear_Callback(hObject, eventdata, handles)
% hObject    handle to clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
global X0;
global X;
global shu;
for i=J:1
    X0(i)=[];
    X(i)=[];
    shu(i)=[];
end
J=0;
cla(handles.axes1);
cla(handles.axes2);
cla(handles.axes3);
set(handles.edit1,'string',''); 

% --- Executes on button press in close.
function close_Callback(hObject, eventdata, handles)
% hObject    handle to close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc;
close all;
close(gcf);
clear;

% --- Executes on button press in play.
function play_Callback(hObject, eventdata, handles)
% hObject    handle to close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global X0;
global J;
for i=1:J
    sound(cell2mat(X0(i,1)),48000);
end


% --- Executes on button press in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global X0;
global J;
fs=48000;
filemame=get(handles.edit2,'string');
audiowrite([filemame '.wav'],cell2mat(X0(J,1)),fs) ;


function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in open.
function open_Callback(hObject, eventdata, handles)
% hObject    handle to open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
J=J+1;
global X0;
[filename pathname] =uigetfile({'*.*';'*.mp3';'*.m4a';'.wav'},'打开音频');
if isequal(filename,0)||isequal(pathname,0)
     msgbox('没有选中文件','出错');
    return;
else
file=[pathname filename];  
[X0{J,1},fs]=audioread(file);
plot(handles.axes1,cell2mat(X0(J,1)));
x=cell2mat(X0(J,1));
 %% ******************端点检测******************
 x = double(x);
x = x / max(abs(x));
%常数设置
FrameLen = 256;%帧长为256点
FrameInc = 80;%帧移为80点
amp1 = 20;%初始短时能量高门限
amp2 = 2;%初始短时能量低门限
zcr1 = 10;%初始短时过零率高门限
zcr2 = 5;%初始短时过零率低门限
maxsilence = 8;  % 8*10ms  = 80ms
%语音段中允许的最大静音长度，如果语音段中的静音帧数未超过此值，则认为语音还没结束；如果超过了
%该值，则对语音段长度count进行判断，若count<minlen，则认为前面的语音段为噪音，舍弃，跳到静音
%状态0；若count>minlen，则认为语音段结束；
minlen  = 15;    % 15*10ms = 150ms
%语音段的最短长度，若语音段长度小于此值，则认为其为一段噪音
status  = 0;     %初始状态为静音状态
count   = 0;     %初始语音段长度为0
silence = 0;     %初始静音段长度为0
%计算过零率
x1=x(1:end-1);
x2=x(2:end);
%分帧
tmp1=enframe(x1,FrameLen,FrameInc);
tmp2=enframe(x2,FrameLen,FrameInc);
signs = (tmp1.*tmp2)<0;
diffs = (tmp1 -tmp2)>0.02;
zcr   = sum(signs.*diffs, 2);%一帧一个值
%计算短时能量
%一帧一个值
%amp = sum(abs(enframe(filter([1 -0.9375], 1, x), FrameLen, FrameInc)), 2);
amp = sum(abs(enframe(x, FrameLen, FrameInc)), 2);
%调整能量门限
amp1 = min(amp1, max(amp)/4);
amp2 = min(amp2, max(amp)/8);
%开始端点检测
%For循环，整个信号各帧比较
%根据各帧能量判断帧所处的阶段
x1 = 0;
x2 = 0;
v_num=0;%记录语音段数
v_Begin=[];%记录所有语音段的起点
v_End=[];%记录所有语音段的终点
%length(zcr)即为帧数
for n=1:length(zcr)
   goto = 0;
   switch status
   case {0,1}                   % 0 = 静音, 1 = 可能开始
      if amp(n) > amp1          % 确信进入语音段
         x1 = max(n-count-1,1);
%          '打印每个x1*FrameInc'
%          x1*FrameInc
         status  = 2;
         silence = 0;
         count   = count + 1;
      elseif amp(n) > amp2 | ... % 可能处于语音段
             zcr(n) > zcr2
         status = 1;
         count  = count + 1;
      else                       % 静音状态
         status  = 0;
         count   = 0;
      end
   case 2,                       % 2 = 语音段
      if amp(n) > amp2 | ...     % 保持在语音段
         zcr(n) > zcr2
         count = count + 1;
      else                       % 语音将结束
         silence = silence+1;
         if silence < maxsilence % 静音还不够长，尚未结束
            count  = count + 1;
         elseif count < minlen   % 语音长度太短，认为是噪声
            status  = 0;
            silence = 0;
            count   = 0;
         else                    % 语音结束
            status  = 3;
         end
      end
   case 3,
      %break;
      %记录当前语音段数据
      v_num=v_num+1;   %语音段个数加一
      count = count-silence/2;
      x2 = x1 + count -1;
      v_Begin(1,v_num)=x1*FrameInc; 
      v_End(1,v_num)=x2*FrameInc;
      %不跳出 数据归零继续往下查找下一段语音
      status  = 0;     %初始状态为静音状态
      count   = 0;     %初始语音段长度为0
      silence = 0;     %初始静音段长度为0
   end
end  

if length(v_End)==0
    x2 = x1 + count -1;
    v_Begin(1,1)=x1*FrameInc; 
    v_End(1,1)=x2*FrameInc;
end
lenafter=0;
for len=1:length(v_End)
    tmp=v_End(1,len)-v_Begin(1,len);
    lenafter=lenafter+tmp;
end
lenafter;
afterEndDet=zeros(lenafter,1);%返回去除静音段的语音信号
beginnum=0;
endnum=0; 
    for k=1:length(v_End)
        tmp=x(v_Begin(1,k):v_End(1,k));
        beginnum=endnum+1;
        endnum=beginnum+v_End(1,k)-v_Begin(1,k);
        afterEndDet(beginnum:endnum)=tmp; 
    end
  plot(handles.axes2,tmp);
   global X;
   X{J,1}=tmp;
end

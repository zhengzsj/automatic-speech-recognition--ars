# Automatic Speech Recognition -ARS

#### 介绍
使用模板匹配或者k-means聚类对数字0-9进行识别，同时实现TCP数据传输等小功能
k-means聚类：
![输入图片说明](GUI2.png)

Julei.m：使用k-means聚类进行识别

Julei.fig：k-means聚类的GUI界面

Cdata.m：计算聚类中心，需要使用语音文件，0-9每个数字5个语音文件，若不是5个需要修改代码

模板匹配：
![输入图片说明](GUI1.png)
yuyinshibie.m：使用模板匹配进行识别

yuyinshibie.fig：使用模板匹配的GUI界面

muban.m：通过语音文件，创建模板（最好最后将数据保存为.mat文件）

dtw.m:dtwa函数
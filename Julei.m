function varargout = Julei(varargin)
% CENGCIJULEI MATLAB code for cengcijulei.fig
%      CENGCIJULEI, by itself, creates a new CENGCIJULEI or raises the existing
%      singleton*.
%
%      H = CENGCIJULEI returns the handle to a new CENGCIJULEI or the handle to
%      the existing singleton*.
%
%      CENGCIJULEI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CENGCIJULEI.M with the given input arguments.
%
%      CENGCIJULEI('Property','Value',...) creates a new CENGCIJULEI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Julei_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Julei_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help cengcijulei

% Last Modified by GUIDE v2.5 04-Jun-2019 05:12:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Julei_OpeningFcn, ...
                   'gui_OutputFcn',  @Julei_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before cengcijulei is made visible.
function Julei_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to cengcijulei (see VARARGIN)

% Choose default command line output for cengcijulei
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes cengcijulei wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Julei_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in cengcijulei.
function cengcijulei_Callback(hObject, eventdata, handles)
% hObject    handle to cengcijulei (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in luyin.
function luyin_Callback(hObject, eventdata, handles)
handles.recObj = audiorecorder;
disp('Start speaking.')
record(handles.recObj); % 开始录音
guidata(hObject,handles);
% hObject    handle to luyin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in clear.
function clear_Callback(hObject, eventdata, handles)
set(handles.edit1,'string',''); 
cla(handles.axes1);
% hObject    handle to clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in receive.
function receive_Callback(hObject, eventdata, handles)
%% 主机
% echotcpip('off')
% echotcpip('on',21000)
t_server=tcpip('0.0.0.0',80,'NetworkRole','server');%与第一个请求连接的客户机建立连接，端口号为80，类型为服务器。
t_server.InputBuffersize=200000;%设置最大缓冲区
fopen(t_server);%打开服务器，直到建立一个TCP连接才返回；
try_times=100;%尝试读取缓冲区数据的次数，可随意设置；
for i=1:try_times
    pause(0.02);%每次读取之前等待0.02s，随意设置
    try     %因为fread（）在缓冲区没有数据的时候读取会报错，因此用try—catch语句忽略这种错误，直到读取到数据。
        data_recv=fread(t_server,t_server.BytesAvailable);%从缓冲区读取数字数据
        %%%%%%%%% data_recv=fscanf(t_server); %接收文本数据
    catch
        t_server.ValuesReceived;%查看读取出的数据数量，如果没有读到，返回0；
    end
end
fclose(t_server);
fs=48000;
for i=1:length(data_recv)
    switch data_recv(i)
        case 0
          n1=get(handles.edit1,'string');
          set(handles.edit1,'string',strcat(n1,'0'));
          [a,fs]=audioread('C:\01 模式识别\E-Vioce\0.wav');
          sound(a,fs) 
          pause(1);
        case 1
          set(handles.edit1,'string',strcat(get(handles.edit1,'string'),'1'));
          [a,fs]=audioread('C:\01 模式识别\E-Vioce\1.wav');
          sound(a,fs)
          pause(1);
        case 2
          set(handles.edit1,'string',strcat(get(handles.edit1,'string'),'2'));
          [a,fs]=audioread('C:\01 模式识别\E-Vioce\2.wav');
          sound(a,fs)
          pause(1);
        case 3
          set(handles.edit1,'string',strcat(get(handles.edit1,'string'),'3'));
          [a,fs]=audioread('C:\01 模式识别\E-Vioce\3.wav');
          sound(a,fs)
          pause(1);
        case 4
          set(handles.edit1,'string',strcat(get(handles.edit1,'string'),'4'));
          [a,fs]=audioread('C:\01 模式识别\E-Vioce\4.wav');
          sound(a,fs) 
          pause(1);
        case 5
          set(handles.edit1,'string',strcat(get(handles.edit1,'string'),'5'));
          [a,fs]=audioread('C:\01 模式识别\E-Vioce\5.wav');
          sound(a,fs);
          pause(1);
        case 6
          set(handles.edit1,'string',strcat(get(handles.edit1,'string'),'6'));
          [a,fs]=audioread('C:\01 模式识别\E-Vioce\6.wav');
          sound(a,fs)
          pause(1);
        case 7
          set(handles.edit1,'string',strcat(get(handles.edit1,'string'),'7'));
          [a,fs]=audioread('C:\01 模式识别\E-Vioce\7.wav');
          sound(a,fs); 
          pause(1);
        case 8
          set(handles.edit1,'string',strcat(get(handles.edit1,'string'),'8'));
          [a,fs]=audioread('C:\01 模式识别\E-Vioce\8.wav');
          sound(a,fs) 
          pause(1);
        case 9
          set(handles.edit1,'string',strcat(get(handles.edit1,'string'),'9'));
          [a,fs]=audioread('C:\01 模式识别\E-Vioce\9.wav');
          sound(a,fs)
          pause(1);
    end
end
% hObject    handle to receive (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in tingzhi.
function tingzhi_Callback(hObject, eventdata, handles)
stop(handles.recObj); % 停止录音
x=getaudiodata(handles.recObj);
guidata(hObject,handles);
disp('End of Recording.');
fs=48000;
% audiowrite('C:\Users\提高效率\Documents\录音\层次聚类\999.wav',x,fs);
%% 端点检测
%幅度归一化到[-1,1]
x = double(x);
x = x / max(abs(x));
% figure,plot(x);
%常数设置
FrameLen = 256;%帧长为256点
FrameInc = 80;%帧移为80点
amp1 = 20;%初始短时能量高门限
amp2 = 2;%初始短时能量低门限
zcr1 = 10;%初始短时过零率高门限
zcr2 = 5;%初始短时过零率低门限
maxsilence = 8;  % 8*10ms  = 80ms
%语音段中允许的最大静音长度，如果语音段中的静音帧数未超过此值，则认为语音还没结束；如果超过了
%该值，则对语音段长度count进行判断，若count<minlen，则认为前面的语音段为噪音，舍弃，跳到静音
%状态0；若count>minlen，则认为语音段结束；
minlen  = 15;    % 15*10ms = 150ms % 语音段的最短长度，若语音段长度小于此值，则认为其为一段噪音
status  = 0;     %初始状态为静音状态
count   = 0;     %初始语音段长度为0
silence = 0;     %初始静音段长度为0
%计算过零率
x1=x(1:end-1);
x2=x(2:end);
%分帧
tmp1=enframe(x1,FrameLen,FrameInc);
tmp2=enframe(x2,FrameLen,FrameInc);
signs = (tmp1.*tmp2)<0;
diffs = (tmp1 -tmp2)>0.02;
zcr   = sum(signs.*diffs, 2);%一帧一个值
%计算短时能量
%一帧一个值
%amp = sum(abs(enframe(filter([1 -0.9375], 1, x), FrameLen, FrameInc)), 2);
amp = sum(abs(enframe(x, FrameLen, FrameInc)), 2);
%调整能量门限
amp1 = min(amp1, max(amp)/4);
amp2 = min(amp2, max(amp)/8);
%开始端点检测
%For循环，整个信号各帧比较
%根据各帧能量判断帧所处的阶段
x1 = 0;
x2 = 0;
v_num=0;%记录语音段数
v_Begin=[];%记录所有语音段的起点
v_End=[];%记录所有语音段的终点
%length(zcr)即为帧数
for n=1:length(zcr)
   goto = 0;
   switch status
   case {0,1}                   % 0 = 静音, 1 = 可能开始
      if amp(n) > amp1          % 确信进入语音段
         x1 = max(n-count-1,1);
%          '打印每个x1*FrameInc'
%          x1*FrameInc
         status  = 2;
         silence = 0;
         count   = count + 1;
      elseif amp(n) > amp2 | ... % 可能处于语音段
             zcr(n) > zcr2
         status = 1;
         count  = count + 1;
      else                       % 静音状态
         status  = 0;
         count   = 0;
      end
   case 2                     % 2 = 语音段
      if amp(n) > amp2 | ...     % 保持在语音段
         zcr(n) > zcr2
         count = count + 1;
      else                       % 语音将结束
         silence = silence+1;
         if silence < maxsilence % 静音还不够长，尚未结束
            count  = count + 1;
         elseif count < minlen   % 语音长度太短，认为是噪声
            status  = 0;
            silence = 0;
            count   = 0;
         else                    % 语音结束
            status  = 3;
         end
      end
   case 3
      %break;
      %记录当前语音段数据
      v_num=v_num+1;   %语音段个数加一
      count = count-silence/2;
      x2 = x1 + count -1;
      v_Begin(1,v_num)=x1*FrameInc; 
      v_End(1,v_num)=x2*FrameInc;
      %不跳出 数据归零继续往下查找下一段语音
      status  = 0;     %初始状态为静音状态
      count   = 0;     %初始语音段长度为0
      silence = 0;     %初始静音段长度为0
   end
end  
if length(v_End)==0
    x2 = x1 + count -1;
    v_Begin(1,1)=x1*FrameInc; 
    v_End(1,1)=x2*FrameInc;
end
lenafter=0;
for len=1:length(v_End)
    tmp=v_End(1,len)-v_Begin(1,len);
    lenafter=lenafter+tmp;
end
lenafter;
afterEndDet=zeros(lenafter,1);%返回去除静音段的语音信号
beginnum=0;
endnum=0; 
    for k=1:length(v_End)
        tmp=x(v_Begin(1,k):v_End(1,k));
        beginnum=endnum+1;
        endnum=beginnum+v_End(1,k)-v_Begin(1,k);
        afterEndDet(beginnum:endnum)=tmp; 
    end
%     figure,plot(tmp);
% end
x=tmp;
%% Mel三角滤波器组参数
fh=fs/2; % fs=8000Hz,fh=4000Hz    语音信号的频率一般在300-3400Hz，所以一般情况下采样频率设为8000Hz即可。
max_melf=2595*log10(1+fh/700);%耳朵响应频率
M=24;%三角滤波器的个数
N=floor(0.03*fs);%设置帧长
i=0:25;
 f=700*(10.^(max_melf/2595*i/(M+1))-1);%将mei频域中的 各滤波器的中心频率 转到实际频率
F=zeros(24,N);
 for m=1:24
     for k=1:N
         i=fh*k/N;
         if (f(m)<=i)&&(i<=f(m+1))
             F(m,k)=(i-f(m))/(f(m+1)-f(m));
         else if (f(m+1)<=i)&&(i<=f(m+2))
                 F(m,k)=(f(m+2)-i)/(f(m+2)-f(m+1));
         else
                 F(m,k)=0;
             end
         end
     end
 end
%  figure,plot((1:N)*fh/N,F);
 %% DCT系数的计算方法
dctcoef=zeros(12,24);
 for k=1:12
   n=1:24;
   dctcoef(k,:)=cos((2*n-1)*k*pi/(2*24));
 end
 %% 预加重
len=length(x);
 alpha=0.98;
 y=zeros(len,1);
 for i=2:len
     y(i)=x(i)-alpha*x(i-1);
 end
 %% MFCC特征参数的求取
 h=hamming(N);%256*1
 num=floor(0.235*N); %帧移
 count=floor((len-N)/num+1);%帧数
 c1=zeros(count,12);
 for i=1:count
     x_frame=y(num*(i-1)+1:num*(i-1)+N);%分帧
     w = x_frame.* h;%
     Fx=abs(fft(w));%     Fx=abs(fft(x_frame));
     s=log(F*Fx.^2);%取对数
     c1(i,:)=(dctcoef*s)'; %离散余弦变换 
 end
%% 求取一阶差分mfcc系数
dtm = zeros(size(c1));
for i=3:size(c1,1)-2
  dtm(i,:) = -2*c1(i-2,:) - c1(i-1,:) + c1(i+1,:) + 2*c1(i+2,:);
end
dtm = dtm/3;
%% 合并
ccc=[c1 dtm];
ccc = ccc(3:size(c1,1)-2,:);
%% 一段语音的所有帧的mfcc参数求均值
me0=mean(ccc);
% me0=me;%储存一下
me0=me0(:,2:20);
%% kmeans聚类中心判别
type=11;
min_=100000;
% load C4
C4=xlsread('C4.xlsx');
for i=1:10
    D(i,1)=dtw(C4(i,:),me0);
    if(D(i,1)<500)
        type=i-1;
        set(handles.edit1,'string',type);
    end
%       if(D(i,1)<min_)
%           min_=D(i,1);
%           type=i;
%       end
end
if type==11
    set(handles.edit1,'string','无法识别');
end
%% 层次聚类
fileFolder='层次聚类\';
dirOutput=dir(strcat(fileFolder,'*'));
fileNames={dirOutput.name};
len = length(fileNames); 
%% 循环读取
for j=3:len
    % 连接路径和文件名得到完整的文件路径
    K_Trace = strcat(fileFolder, fileNames(j));
   eval(['[x,fs]','=','audioread(K_Trace{1,1})',';']);
%% 端点检测
%幅度归一化到[-1,1]
x = double(x);
x = x / max(abs(x));
% figure,plot(x);
%常数设置
FrameLen = 256;%帧长为256点
FrameInc = 80;%帧移为80点
amp1 = 20;%初始短时能量高门限
amp2 = 2;%初始短时能量低门限
zcr1 = 10;%初始短时过零率高门限
zcr2 = 5;%初始短时过零率低门限
maxsilence = 8;  % 8*10ms  = 80ms
%语音段中允许的最大静音长度，如果语音段中的静音帧数未超过此值，则认为语音还没结束；如果超过了
%该值，则对语音段长度count进行判断，若count<minlen，则认为前面的语音段为噪音，舍弃，跳到静音
%状态0；若count>minlen，则认为语音段结束；
minlen  = 15;    % 15*10ms = 150ms
%语音段的最短长度，若语音段长度小于此值，则认为其为一段噪音
status  = 0;     %初始状态为静音状态
count   = 0;     %初始语音段长度为0
silence = 0;     %初始静音段长度为0
%计算过零率
x1=x(1:end-1);
x2=x(2:end);
%分帧
tmp1=enframe(x1,FrameLen,FrameInc);
tmp2=enframe(x2,FrameLen,FrameInc);
signs = (tmp1.*tmp2)<0;
diffs = (tmp1 -tmp2)>0.02;
zcr   = sum(signs.*diffs, 2);%一帧一个值,2表示按行求和
%计算短时能量
%一帧一个值
%amp = sum(abs(enframe(filter([1 -0.9375], 1, x), FrameLen, FrameInc)), 2);
amp = sum(abs(enframe(x, FrameLen, FrameInc)), 2);
%调整能量门限
amp1 = min(amp1, max(amp)/4);
amp2 = min(amp2, max(amp)/8);
%开始端点检测
%For循环，整个信号各帧比较
%根据各帧能量判断帧所处的阶段
x1 = 0;
x2 = 0;
v_num=0;%记录语音段数
v_Begin=[];%记录所有语音段的起点
v_End=[];%记录所有语音段的终点
%length(zcr)即为帧数
for n=1:length(zcr)
   goto = 0;
   switch status
   case {0,1}                   % 0 = 静音, 1 = 可能开始
      if amp(n) > amp1          % 确信进入语音段
         x1 = max(n-count-1,1);
%          '打印每个x1*FrameInc'
%          x1*FrameInc
         status  = 2;
         silence = 0;
         count   = count + 1;
      elseif amp(n) > amp2 | ... % 可能处于语音段
             zcr(n) > zcr2
         status = 1;
         count  = count + 1;
      else                       % 静音状态
         status  = 0;
         count   = 0;
      end
   case 2                     % 2 = 语音段
      if amp(n) > amp2 | ...     % 保持在语音段
         zcr(n) > zcr2
         count = count + 1;
      else                       % 语音将结束
         silence = silence+1;
         if silence < maxsilence % 静音还不够长，尚未结束
            count  = count + 1;
         elseif count < minlen   % 语音长度太短，认为是噪声
            status  = 0;
            silence = 0;
            count   = 0;
         else                    % 语音结束
            status  = 3;
         end
      end
   case 3
      %break;
      %记录当前语音段数据
      v_num=v_num+1;   %语音段个数加一
      count = count-silence/2;
      x2 = x1 + count -1;
      v_Begin(1,v_num)=x1*FrameInc; 
      v_End(1,v_num)=x2*FrameInc;
      %不跳出 数据归零继续往下查找下一段语音
      status  = 0;     %初始状态为静音状态
      count   = 0;     %初始语音段长度为0
      silence = 0;     %初始静音段长度为0
   end
end  
if length(v_End)==0
    x2 = x1 + count -1;
    v_Begin(1,1)=x1*FrameInc; 
    v_End(1,1)=x2*FrameInc;
end
lenafter=0;
for len=1:length(v_End)
    tmp=v_End(1,len)-v_Begin(1,len);
    lenafter=lenafter+tmp;
end
lenafter;
afterEndDet=zeros(lenafter,1);%返回去除静音段的语音信号
beginnum=0;
endnum=0; 
    for k=1:length(v_End)
        tmp=x(v_Begin(1,k):v_End(1,k));
        beginnum=endnum+1;
        endnum=beginnum+v_End(1,k)-v_Begin(1,k);
        afterEndDet(beginnum:endnum)=tmp; 
    end
%     figure,plot(tmp);
% end
x=tmp;
%% Mel三角滤波器组参数
fh=fs/2; % fs=8000Hz,fh=4000Hz    语音信号的频率一般在300-3400Hz，所以一般情况下采样频率设为8000Hz即可。
max_melf=2595*log10(1+fh/700);%耳朵响应频率
M=24;%三角滤波器的个数
N=floor(0.03*fs);%设置帧长
i=0:25;
 f=700*(10.^(max_melf/2595*i/(M+1))-1);%将mei频域中的 各滤波器的中心频率 转到实际频率
F=zeros(24,N);
 for m=1:24
     for k=1:N
         i=fh*k/N;
         if (f(m)<=i)&&(i<=f(m+1))
             F(m,k)=(i-f(m))/(f(m+1)-f(m));
         else if (f(m+1)<=i)&&(i<=f(m+2))
                 F(m,k)=(f(m+2)-i)/(f(m+2)-f(m+1));
         else
                 F(m,k)=0;
             end
         end
     end
 end
%  figure,plot((1:N)*fh/N,F);
 %% DCT系数的计算方法
dctcoef=zeros(12,24);
 for k=1:12
   n=1:24;
   dctcoef(k,:)=cos((2*n-1)*k*pi/(2*24));
 end
 %% 预加重
len=length(x);
 alpha=0.98;
 y=zeros(len,1);
 for i=2:len
     y(i)=x(i)-alpha*x(i-1);
 end
 %% MFCC特征参数的求取
 h=hamming(N);%256*1
 num=floor(0.235*N); %帧移
 count=floor((len-N)/num+1);%帧数
 c1=zeros(count,12);
 for i=1:count
     x_frame=y(num*(i-1)+1:num*(i-1)+N);%分帧
     w = x_frame.* h;%
     Fx=abs(fft(w));%     Fx=abs(fft(x_frame));
     s=log(F*Fx.^2);%取对数
     c1(i,:)=(dctcoef*s)'; %离散余弦变换 
 end
%% 求取一阶差分mfcc系数
dtm = zeros(size(c1));
for i=3:size(c1,1)-2
  dtm(i,:) = -2*c1(i-2,:) - c1(i-1,:) + c1(i+1,:) + 2*c1(i+2,:);
end
dtm = dtm/3;
%% 合并
ccc=[c1 dtm];
ccc = ccc(3:size(c1,1)-2,:);
%% 一段语音的所有帧的mfcc参数求均值
me1(j,:)=mean(ccc);
end
me1=me1(3:end,2:20);
me=[me1;me0];
%%
Y=pdist(me);              % 计算样本点之间的欧氏距离
Y=squareform(Y);  
Z=linkage(Y);             % 用最短距离法创建系统聚类树?
% C2=cophenet(Z,Y)        % //0.94698
T=cluster(Z,10);           % 聚类结果
dendrogram(Z,0,'ColorThreshold',20);
% hObject    handle to tingzhi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
